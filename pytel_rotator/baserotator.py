# baserotator.py

import threading
from enum import Enum

from astropy.coordinates import SkyCoord,EarthLocation,AltAz
from astropy.time import Time
from astropy import units as u

from pytel.interfaces import IRotator, IMotionDevice

from pytel.modules import timeout, PytelModule
from pytel.utils.threads import LockWithAbort

class BaseRotator(PytelModule, IRotator):

	def __init__(self, lat: float, lon: float, *args, **kwargs):
		PytelModule.__init__(self, *args, **kwargs)
		self._position['ra']  = None
		self._position['dec'] = None
		self._angle  = None
		self._offset = 0.
		self._status = IMotionDevice.Status.UNKNOWN

		# initialize handy objects for parallactic_angle()
		# since astropy.coordinates is very slow
		self._coord	= SkyCoord(0.*u.deg,0.*u.deg,frame='icrs')
		self._location = EarthLocation(lat=lat*u.deg, lon=long*u.deg)
		self._altaz	= AltAz (Time.now(),location=self._location)

		# some multi-threading stuff
		self._lock_moving = threading.Lock()
		self._abort_move = threading.Event()

	def init(self, *args, **kwargs) -> bool:
		raise NotImplementedError

	def abort (self, *args, **kwargs) -> bool:
		raise NotImplementedError

	def status(self, *args, **kwargs) -> dict:
		raise NotImplementedError

	# ---- rotator methods

	@timeout(60000)
	def move(self, angle: float, *args, **kwargs) -> bool:
		# acquire lock
		with LockWithAbort(self._lock_moving, self._abort_move):
			# move rotator
			return self._move(angle, abort_event=self._abort_move)

	def _move(self, angle: float, abort_event: threading.Event) -> bool:
		raise NotImplementedError

	def reset_offset(self, *args, **kwargs) -> bool:
		self._offset = 0.

	def set_offset(self, dangle: float, *args, **kwargs) -> bool:
		self._offset = dangle

	@timeout(60000)
	def track_altaz(self, ra: float, dec: float, *args, **kwargs) -> bool:
		"""
		Used by derotators on Alt-Az telescopes to track at a constant parallactic ange.

		Slew to the correct parallactic angle for the position (ra,dec) and then
		track, continuously updating the correct angle.
		"""
		# acquire lock
		with LockWithAbort(self._lock_moving, self._abort_move):
			# track using standard position angle geometry
			return self._track_altaz(ra, dec, abort_event=self._abort_move)

	def _track_altaz(self, ra: float, dec: float, abort_event: threading.Event) -> bool:
		raise NotImplementedError

	def parallactic_angle (ra: float, dec: float, t=None, *args, **kwargs) -> float:
		"""
		Calculates parallactic angle [deg] between the great circle through
		an object with a given ra,dec and the zenith and the hour circle
		at the present time and observer position.  The angle is defined
		by the spherical triangle with corners pole-zenith-object and
		sides 90deg-latitude, zenith_distance,90deg-dec.
		If time=None, the current time is used.
		"""
		# get time
		if t is None :
			t = Time.now()

		# transform
		self._coord.ra  = ra *u.deg
		self._coord.dec = dec*u.deg
		self._altaz.obstime = t
		altaz = self._coord.transform_to(self._altaz)

		# solve spherical triangle
		alt = altaz.alt.to(u.rad).value			# RADIANS
		zdist = 0.5*np.pi-alt				# RADIANS
		lat   = self._location.lat.to(u.rad).value	# RADIANS
		co_lat = 0.5*np.pi-self._location.lat.to(u.rad).value
		co_dec = 0.5*np.pi-dec*np.pi/180.
		cospa = (np.cos(co_lat)-np.cos(co_dec)*np.cos(zdist)) \
					/  (np.sin(co_dec)*np.sin(zdist))
		return 180.*np.arccos(cospa)/np.pi


__all__ = ['BaseRotator']

