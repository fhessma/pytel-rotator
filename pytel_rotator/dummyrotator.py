# dummyrotator.py

import logging
import threading
import time
from astropy.coordinates import SkyCoord
from astropy import units as u

from pytel.interfaces import IRotator, IFitsHeaderProvider

from pytel_rotator import baserotator

from pytel.modules import timeout
from pytel.utils.threads import LockWithAbort


log = logging.getLogger(__name__)


class DummyRotator(baserotator.BaseRotator, IFitsHeaderProvider.IFitsHeaderProvider):

	@timeout(60000)
	def init(self, *args, **kwargs) -> bool:
		time.sleep(5.)
		self.rotator_status = IRotator.RotatorStatus.IDLE
		return True

	def status (self, *args, **kwargs) -> dict:
		stat =	{
			'ra': self._position['ra'],
			'dec': self._position['dec'],
			'angle': self._angle,
			'offset': self._offset,
			'status': self.rotator_status
			}
		return stat

	def get_fits_headers(self, *args, **kwargs) -> dict:
		hdr =	{
			'ROT-RA':   (self._position['ra'], 'Declination used by derotator [degrees]'),
			'ROT-DEC':  (self._position['dec'], 'Right ascension used by derotator [degrees]'),
			'ROT-ANGLE':(self._angle, 'Rotator angle [deg]'),
			'ROT-OFF':  (self._offset, 'Rotator angle offset [deg]')
			}

		# to sexagesimal
		if 'OBJRA' in hdr and 'OBJDEC' in hdr:
			# create sky coordinates
			c = SkyCoord(ra=hdr['OBJRA'][0] * u.deg, dec=hdr['OBJDEC'][0] * u.deg, frame='icrs')

			# convert
			hdr['ROT-RA'] = (str(c.ra.to_string(sep=':', unit=u.hour, pad=True)), 'Right ascension used by derotator')
			hdr['ROT-DEC'] = (str(c.dec.to_string(sep=':', unit=u.deg, pad=True)), 'Declination used by derotator')

		# finish
		return hdr

	def abort (self, *args, **kwargs) ->  bool:
		self.rotator_status = IRotator.RotatorStatus.IDLE
		return True

	def _track_altaz(self, ra: float, dec: float, abort_event: threading.Event) -> bool:
		""" Simulation of a derotator.  """

		speed = 5. # DEG/S, MAX SPEED OF ROTATOR

		# start slewing
		log.info("Slewing derotator to parallactic angle of RA=%.2f, Dec=%.2f...", ra, dec)
		self.rotator_status = IRotator.RotatorStatus.SLEWING
		a = self._angle
		aend = self.parallactic_angle (ra,dec)

		# simulate rotation slew
		da = 0.1/speed # AMPLITUDE (WITHOUT SIGN) FOR 0.1s TIMESTEPS
		while True :
			self._angle = a

			# error?
			if self.rotator_status == IRotator.RotatorStatus.ERROR:
				return False

			# abort?
			elif abort_event.is_set() or self.rotator_status == IRotator.RotatorStatus.IDLE:
				self.rotator_status = IRotator.RotatorStatus.IDLE
				return False

			# slewing
			elif self.rotator_status == IRotator.RotatorStatus.SLEWING:
				if np.abs(a-aend) < da :
					self.rotator_status = IRotator.RotatorStatus.TRACKING
					self._position['ra'] = ra
					self._position['dec'] = dec
					log.info('Reached destination, start tracking')
				else :
					a += 1.001*da*np.sgn(aend-a)

			# move
			elif np.abs(a-aend) > da :
				a += 0.999*da*np.sgn(aend-a)

			# find new parallactic angle (e.g. during tracking)
			aend = self.parallactic_angle (ra,dec)

			# sleep a little
			time.sleep(0.1)

		return True

__all__ = ['DummyRotator']

