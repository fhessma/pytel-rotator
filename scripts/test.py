# pytel-rotator/scripts/test.py

"""
Tests a generic Nasmyth derotator.
"""

import asyncio
import logging
import math

import numpy as np

from pytel import PytelModule
from pytel-rotator import IRotator

log = logging.getLogger(__name__)


class TestDerotator(PytelModule):
	def __init__(self, *args, **kwargs):
		PytelModule.__init__(self, *args, **kwargs)
		self._rotator = None

	@classmethod
	def default_config(cls):
		cfg = super(TestDerotator, cls).default_config()
		cfg['ra'] = 11.111
		cfg['dec'] = +22.222
		return cfg

	def link_modules(self):
		# get rotator
		name = self.config['rotator'] if 'rotator' in self.config else None
		if not name or name not in self.linked_modules or not isinstance(self.linked_modules[name], IRotator):
			raise ValueError('Invalid rotator "%s" in config.' % name)
		self._rotator = self.linked_modules[name]

	def run(self):
		try:
					log.info('Starting derotator...')
					self._rotator.track_nasymth(ra,dec)
		except asyncio.CancelledError:
			pass

		except:
			# on all other exceptions, log them and continue
			log.exception("An error occurred while doing blind flats")

		finally:
		log.info("Finished test!")

__all__ = ['TestDerotator']



